const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    mode: 'development',
    entry:  {
      guitarappvue: __dirname + "/app/javascript/guitarappvue.js",
      vue: __dirname + "/app/javascript/vue.js",
    },

    output: {
      //path: __dirname + "/public",
      path: __dirname + "/build",
      filename: "[name].js"
    },
  //},
  module: {
    rules: [
      {
       test: /\.css$/,
       loader: [ 'style-loader', 'css-loader' ]
      }
    ],
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015'],
          plugins: ['transform-runtime']
        }
      }
    ]
  },
  plugins: [
    new CopyWebpackPlugin([
      { from: './app/index.html', to: "index.html" },
      { from: './app/register.html', to: "register.html" },
      { from: './app/contact.html', to: "contact.html" },
      { from: './app/guitarlogo.png', to: "guitarlogo.png" },
      { from: './app/modal.css', to: "modal.css" },
    ])
  ],
}
