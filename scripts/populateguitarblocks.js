module.exports = function(callback) {

    var Web3 = require('web3');
    var provider = new Web3.providers.HttpProvider("http://localhost:8545");
    var contract = require('truffle-contract');
    var GuitarBlock = contract(require('../build/contracts/GuitarBlock.json'));
    var web3 = new Web3(provider);

    /** --------------------------- Seteamos los propietarios ------------------------- */

    // Seteamos las cuentas de Ganache

    const _creator = web3.eth.accounts[0];
    const _owner1 = web3.eth.accounts[1];
    const _owner2 = web3.eth.accounts[2];
    const _owner3 = web3.eth.accounts[3];
    const _owner4 = web3.eth.accounts[4];
    const _owner5 = web3.eth.accounts[5];
    const _owner6 = web3.eth.accounts[6];
    const _owner7 = web3.eth.accounts[7];

    // Seteamos las marcas de los instrumentos

    const _manufacturer1 = "Fender";
    const _manufacturer2 = "Yamaha";
    const _manufacturer3 = "MusicMan";
    const _manufacturer4 = "Ibanez";
    const _manufacturer5 = "Rickenbacker";
    const _manufacturer6 = "Gibson";
    const _manufacturer7 = "Martin";

    // Seteamos los respectivos números de serie

    const _serialNumber1 = "SBC973528365";
    const _serialNumber2 = "M1024";
    const _serialNumber3 = "LTS7300927";
    const _serialNumber4 = "R76HGTEUR7";
    const _serialNumber5 = "VGHR8987IHKH";
    const _serialNumber6 = "98UYDGE";
    const _serialNumber7 = "63U00927";

    const _price1 = 1.0;
    const _price2 = .001;
    const _price3 = .02;
    const _price4 = 10.0;
    const _price5 = 1.01;
    const _price6 = 1.002;


    GuitarBlock.setProvider(provider); // Seteamos al provider como localhost
    GuitarBlock.defaults({from: _creator, gas: 900000 });

    const populateDeeds = async () => {
      let deed = await GuitarBlock.deployed();
      let name = await deed.name();

      try {
        // These will appear when you click the "All Guitars" link or when the index.html first loads.
        await deed.create(_serialNumber1, _manufacturer1, _owner1);
        await deed.create(_serialNumber2, _manufacturer2, _owner2);
        await deed.create(_serialNumber3, _manufacturer3, _owner3);
        await deed.create(_serialNumber4, _manufacturer4, _owner4);
        await deed.create(_serialNumber5, _manufacturer5, _owner5);
        await deed.create(_serialNumber6, _manufacturer6, _owner6);
        await deed.create(_serialNumber7, _manufacturer7, _owner7);
        // Optionally add your PERSONAL Guitar(s) here, so the "My Guitars" link will work.
        //await deed.create("My$3r1AlnuM53R", "MARCA INSTRUMENTO", "0xbf00309c721accdf1f44f60c601b99bf2863b338");
        //await deed.create("My$3r1AlnuM53R", "MARCA INSTRUMENTO", "0xbf00309c721accdf1f44f60c601b99bf2863b338");
      } catch (error) {
        console.log(error.message);
      }
    }
populateDeeds();
}
