module.exports = function(callback) {

var Web3 = require('web3');
var provider = new Web3.providers.HttpProvider("http://localhost:8545");
var contract = require('truffle-contract');
var GuitarBlock = contract(require('../build/contracts/GuitarBlock.json'));
var web3 = new Web3(provider);

const _creator = web3.eth.accounts[0];

GuitarBlock.setProvider(provider);
GuitarBlock.defaults({from: _creator, gas: 900000 });

const displayGuitars = async () => {
  let deed = await GuitarBlock.deployed();
  let deedIds = await deed.ids();

  const FIELD_NAME  = 0
  const FIELD_SERIAL_NUMBER = 1
  const FIELD_MANUFACTURER = 2
  const FIELD_DATE_CREATED = 4
  const FIELD_DATE_DELETED = 5

  let guitarStructs = []
  for (let i = 0; i < deedIds.length; i++) {
    var deedId = deedIds[i];
    var guitarBlock = await deed.deeds(deedId);
    const guitar = {
        name:  web3.toAscii(guitarBlock[FIELD_NAME]),
        serialNumber: web3.toAscii(guitarBlock[FIELD_SERIAL_NUMBER]),
        manufacturer: web3.toAscii(guitarBlock[FIELD_MANUFACTURER]),
        dateCreated: guitarBlock[FIELD_DATE_CREATED],
        dateDeleted: guitarBlock[FIELD_DATE_DELETED]
    }
    guitarStructs.push(guitar)
  }

  console.log('guitarStructs =', guitarStructs)

}

displayGuitars();

}
