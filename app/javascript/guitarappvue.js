// Import libraries we need.
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract'
import Vue from './vue'
import guitarmanufacturersfromfile from './guitarmanufacturers.json'
import VModal from 'vue-js-modal'
import moment from 'moment'

Vue.use(VModal, { dynamic: true })
//import Modal from '../Modal.vue'

var GuitarBlock = contract(require('../../build/contracts/GuitarBlock.json'));

window.app = app;

// register modal component
Vue.component('modal', {
  template: '#modal-details-template',
  methods: {
    emit: function() {
      this.$emit('event_child', 1)
    }
  },
});

// register modal component
Vue.component('modal2', {
  template: '#modal-my-details-template',
  methods: {
    emit: function() {
      this.$emit('event_child', 1)
    }
  },
});

var app = new Vue({
  el: '#app',
  data: {
    contractAddress: '0x5750f35B760C544F599Ec6e5Ae3C075aef9DF952',
    userAccount: '',
    nametag: '',
    status: '',
    message: '',
    allguitars: [],
    myguitars: [],
    singleGuitar: '',
    guitarlist: [],
    manufacturers: [],
    web3Enabled: false,
    web3Injected: false,
    networkLabel: '',
    // display controls
    search: '',
    showDetailsModal: false,
    showMyDetailsModal: false,
    guitarManufacturerSelected: false,
    pooFileLoaded: false,
    pooFileSelected: false,
    displayRegistrationComponents: true,
    showSpinner: false,
    showUploadSpinner: false,
    // specific guitar details
    guitarOwner: '',
    guitarSerialNumber: '',
    guitarId: '',
    guitarManufacturer: '000', // default
    guitarDateCreated: '',
    guitarUrl: '',
    // miscellaneous
    newOwnerAddress: '',
    processingMessage: ''
  },
  beforeCreate: function () {
    console.log("beforeCreate...");
  },
  created: function () {
    console.log("created...");
  },
  beforeMount: function () {
    console.log("beforeMount...");
  },
  mounted:function(){
    console.log("mounted...");

    var accountInterval = setInterval(function() {
      if (this.web3Injected == true) {
        if (web3.eth.accounts[0] !== this.userAccount) {
          this.userAccount = web3.eth.accounts[0];
          updateIupdateInterfacenterface();
        }
      }
    }, 500);

    this.initWeb3();
    if (this.web3Enabled == true) {
      this.initAccounts();
      this.initContract();
      this.loadAllGuitars();
      this.initManufacturers();
      //alert("performed mounted functions");
    }
  },
  beforeUpdate:function(){
    console.log("beforeUpdate...");
  },
  updated:function() {
    console.log("updated...");
  },
  activated:function() {
    console.log("activated...");
  },
  methods:{
    updateInterface:function() {
      window.location.reload(true);
    },
    loadCurrentProvider:function () {

    },
    initWeb3:function() {
      // Checking if Web3 has been injected by the browser (Mist/MetaMask)
      let self = this;
      if (typeof web3 !== 'undefined') {
        console.warn("Using injected web3")
        this.web3Injected = true;
        // Use Mist/MetaMask's provider
        window.web3 = new Web3(web3.currentProvider);
        window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
        web3.eth.defaultAccount = web3.eth.accounts[0];
      }
      var networkId = web3.version.network;
      console.log('networkId: ' + networkId);

      switch (networkId) {
        case "1":
          this.networkLabel = "";
          break;
        case "2":
          this.networkLabel = "You are on the Morden Network - Please switch to Mainnet";
          break;
        case "3":
          this.networkLabel = "You are on the Ropsten Network - Please switch to Mainnet";
          break;
        case "4":
          this.networkLabel = "You are on the Rinkeby Network - Please switch to Mainnet";
          break;
        case "42":
          this.networkLabel = "You are on the Kovan Network - Please switch to Mainnet";
          break;
        default:
          this.networkLabel = "";
      }
      this.web3Enabled = true;
    },
    initAccounts:function(){
      let self = this;
      GuitarBlock.setProvider(web3.currentProvider);
      this.userAccount = web3.eth.accounts[0];
      console.log(web3.eth.accounts);
    },
    initContract:function(){
      let self = this;
      const loadContract = async () => {
        let deed = await GuitarBlock.at(this.contractAddress);
        let name = await deed.name();
        this.nametag = name;
      }
      loadContract();
    },
    isMobile:function() {
      var userAgent = window.navigator.userAgent,
          platform = window.navigator.platform,
          macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
          windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
          iosPlatforms = ['iPhone', 'iPad', 'iPod'],
          os = null;

      if (macosPlatforms.indexOf(platform) !== -1) {
        return false;
      } else if (iosPlatforms.indexOf(platform) !== -1) {
        return true;
      } else if (windowsPlatforms.indexOf(platform) !== -1) {
        return false;
      } else if (/Android/.test(userAgent)) {
        return true;
      } else if (!os && /Linux/.test(platform)) {
        return false;
      }
      return false;
    },
    loadGuitarWithId: async function(deedId) {
      let self = this;
      const loadGuitar = async (deedId) => {
        let deed = await GuitarBlock.at(this.contractAddress);
        const FIELD_NAME  = 0
        const FIELD_SERIAL_NUMBER = 1
        const FIELD_MANUFACTURER = 2
        const FIELD_DATE_CREATED = 3
        const FIELD_DATE_DELETED = 5

        var guitarBlock = await deed.deeds(deedId);
        try {
          var guitarBlock = await deed.ownerOf(deedId);
        } catch(error) {
          // probably a deleted token and therefore has no owner.
          console.log(error);
          return error;
        }
        var url = await deed.deedUri(deedId);
        const guitar = {
          id: deedId,
          name:  web3.toAscii(guitarBlock[FIELD_NAME]),
          serialNumber: web3.toAscii(guitarBlock[FIELD_SERIAL_NUMBER]),
          manufacturer: web3.toAscii(guitarBlock[FIELD_MANUFACTURER]).replace(/\u0000/g, ''),
          dateCreated: moment.unix(guitarBlock[FIELD_DATE_CREATED]).format("DD/MM/YYYY HH:mm:ss"),
          dateDeleted: guitarBlock[FIELD_DATE_DELETED],
          owner: guitarOwner,
          guitarUrl: url
        }
        this.singleGuitar = guitar;
      }
      await loadGuitar(deedId);
    },
    loadAllGuitars: function() {
      let self = this;
      this.allguitars.length=0;
      const loadGuitars = async () => {
        let deed = await GuitarBlock.at(this.contractAddress);
        let deedIds = await deed.ids();

        const FIELD_NAME  = 0
        const FIELD_SERIAL_NUMBER = 1
        const FIELD_MANUFACTURER = 2
        const FIELD_DATE_CREATED = 3
        const FIELD_DATE_DELETED = 5

        for (let i = 0; i < deedIds.length; i++) {
          var deedId = deedIds[i];
          var guitarBlock = await deed.deeds(deedId);
          try {
            var guitarOwner = await deed.ownerOf(deedId);
          } catch(error) {
            // probably a deleted token and therefore has no owner.
            continue;
          }
          var url = await deed.deedUri(deedId);
          const guitar = {
            id: deedId,
            name:  web3.toAscii(guitarBlock[FIELD_NAME]),
            serialNumber: web3.toAscii(guitarBlock[FIELD_SERIAL_NUMBER]),
            manufacturer: web3.toAscii(guitarBlock[FIELD_MANUFACTURER]).replace(/\u0000/g, ''),
            dateCreated: moment.unix(guitarBlock[FIELD_DATE_CREATED]).format("DD/MM/YYYY HH:mm:ss"),
            dateDeleted: guitarBlock[FIELD_DATE_DELETED],
            owner: guitarOwner,
            guitarUrl: url
          }
          if (web3.isAddress(guitarOwner)) {
            this.allguitars.push(guitar);
          }
        }
      }
      loadGuitars();
      this.guitarlist = this.allguitars;
    },
    lookupManufacturerLabel: function (value1) {
      var i;
      for (i = 0; i < this.manufacturers.length; i++) {
        var value2 = this.manufacturers[i].value;
        if (value1.trim() == value2.trim()) {
          return this.manufacturers[i].text;
        }
      }
      return value1;
    },
    guitarLabel: function (guitar) {
      var i;
      for (i = 0; i < this.manufacturers.length; i++) {
        var value1 = guitar.manufacturer;
        var value2 = this.manufacturers[i].value;
        if (value1.trim() == value2.trim()) {
          return this.manufacturers[i].text;
        }
      }
      return value1;
    },
    initManufacturers: function() {
      this.manufacturers = guitarmanufacturersfromfile;
    },
    showMyGuitars:function() {
      this.initAccounts();
      this.myguitars.length = 0;
      for (let index = 0; index < this.allguitars.length; ++index) {
        let guitar = this.allguitars[index];
        if (guitar.owner == this.userAccount) {
          this.myguitars.push(guitar);
        }
      }
      this.guitarlist = this.myguitars;
    },
    showAllGuitars:function() {
      this.myguitars.length = 0;
      this.guitarlist = this.allguitars;
    },
    showGuitarDetails:function(guitar) {
      // Not sure why this has to be done.
      this.initAccounts();
      this.guitarId = guitar.id;
      this.guitarOwner = guitar.owner;
      this.guitarSerialNumber = guitar.serialNumber;
      this.guitarManufacturer = guitar.manufacturer;
      this.guitarDateCreated =  guitar.dateCreated;
      this.guitarUrl = guitar.guitarUrl;
      if (this.userAccount == guitar.owner) {
        this.showMyDetailsModal=true;
        this.displayRegistrationComponents=true;
        this.processingMessage = ""
      }
      else {
        this.showDetailsModal=true;
      }
    },
    displayMetaData:function() {
      window.open(this.guitarUrl, "proofofownershipwindow", "location=yes,height=570,width=520,scrollbars=yes,status=yes");
    },
    confirmRegistration:function() {
      this.initAccounts();
      // HACK ALERT: prepend 'S' if serialNumber does not contain a letter.
      // this is due to a bug in the contract.

      var letter = /.*[a-zA-Z].*/;
      //var letter = /^[a-zA-Z]+$/;
      if (!this.guitarSerialNumber.match(letter))  {
        this.guitarSerialNumber = 'S' + this.guitarSerialNumber;
      }
      this.showDetailsModal = true;
    },
    deleteGuitarDeed: function() {
      const destroyDeed = async () => {
        var self = this;
        GuitarBlock.defaults({from: this.userAccount, gas: 900000 });
        let deed = await GuitarBlock.at(this.contractAddress);
        this.processingMessage = "Eliminando Guitarra. Esto puede tardar un rato...";
        this.showSpinner=true;
        this.displayRegistrationComponents = false;
        this.sleep(1000);
        try {
          let result = await deed.destroy(this.guitarId);
        } catch (error) {
          console.log(error.message);
          this.processingMessage = error.message;
          alert(error.message);
          this.showSpinner=false;
          this.displayRegistrationComponents = true;
          return;
        }
        this.processingMessage = "Tu guitarra ha sido eliminada!";
        this.guitarOwner = this.newOwnerAddress;
        this.showSpinner=false;
        this.displayRegistrationComponents = false;
        this.initAccounts();
        this.loadAllGuitars();
      }

      // Not sure why this has to be done.
      this.initAccounts();

      if (!destroyDeed()) {
        return;
      }
    },
    verifyOwnership: async function(deedId) {
      var self = this;
      this.initAccounts();
      try {
        let guitar = await this.loadGuitarWithId(deedId);
        if (this.singleGuitar) {
          this.showGuitarDetails(this.singleGuitar);
        }
        else {
          alert("GUitar with deedid " + deedId + " not found");
        }
      }
      catch(error) {
        alert(error + ": No guitar found for deed ID: " + deedId);
      }
    },
    transferOwnership: function() {
      const transfer = async () => {
        var self = this;
        GuitarBlock.defaults({from: this.userAccount, gas: 900000 });
        let deed = await GuitarBlock.at(this.contractAddress);
        this.displayRegistrationComponents=false;
        this.processingMessage = "Transferring guitar deed to " + this.newOwnerAddress + ". This may take a while...";
        this.showSpinner = true;
        try {
          //alert("creating guitar deed with "  + this.guitarSerialNumber + " " +  this.guitarManufacturer + " " +  this.userAccount);
          let result = await deed.transfer(this.newOwnerAddress, this.guitarId);
        } catch (error) {
          console.log(error.message);
          this.processingMessage = error.message;
          alert(error.message);
          this.displayRegistrationComponents=true;
          this.showSpinner = false;
          return true;
        }
        this.processingMessage = "Congratulations!  Your guitar has been transferred to " + this.newOwnerAddress + "!";
        this.showSpinner = false;
        this.guitarOwner = this.newOwnerAddress;
        return true;
      }

      // Not sure why this has to be done.
      this.initAccounts();

      if (!web3.isAddress(this.newOwnerAddress)) {
        var errorMsg = "Not a valid address!";
        console.log(errorMsg);
        this.processingMessage = errorMsg;
        return true;
      }

      if (!transfer()) {
        return true;
      }
    },
    registerGuitar: function() {
      this.showDetailsModal = false;
      //alert("registering guitar")
      const registerGuitarOnBlockchain = async () => {
        var self = this;
        GuitarBlock.defaults({from: this.userAccount, gas: 900000 });
        let deed = await GuitarBlock.at(this.contractAddress);
        console.log(deed);
        this.processingMessage = "Registering guitar on the blockchain. This may take a while...";
        this.showSpinner = true;
        try {
          console.log("hola");
          alert("creating guitar with "  + this.guitarSerialNumber + " " +  this.guitarManufacturer + " "  +  this.userAccount);
          let result = await deed.create(this.guitarSerialNumber, this.guitarManufacturer, this.userAccount);
          console.log(result);
        } catch (error) {
          console.log(error.message);
          this.showSpinner = false;
          error.message = "You must be logged into MetaMask for this feature.";
          this.processingMessage = error.message;
          alert(error.message);
          return false;
        }
        this.processingMessage = "Congratulations!  Your guitar has been registered on the blockchain.";
        this.showSpinner = false;
        this.clearRegistrationForm();
        return true;
      }

      // Not sure why this has to be done.
      this.initAccounts();

      if (!registerGuitarOnBlockchain()) {
        return;
      }
    },
    displayPooExample:function() {
      window.open("poofileexample.jpg", "poofileexamplewindow", "titlebar=no,location=no,height=570,width=520,scrollbars=yes,status=no");
    },
    clearRegistrationForm:function() {
      this.pooFileSelected = false;
      this.guitarManufacturer = '000';
      this.guitarSerialNumber = '';
      this.showDetailsModal = false;
      this.guitarManufacturerSelected = false;
      this.pooFileLoaded = false;
      this.processingMessage = '';
    },
    sleep:function(milliseconds) {
      var start = new Date().getTime();
      for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
          break;
        }
      }
    }
  },
  computed: {
    filteredGuitars: function() {
      return this.guitarlist.filter((guitar) => {
        var searchString = this.search.toLowerCase();
        var label = this.guitarLabel(guitar).toLowerCase();
        return (label.match(searchString) || guitar.serialNumber.toLowerCase().match(searchString));
      });
    }
  }
})
